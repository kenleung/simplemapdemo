package com.kenleung.simplemapdemo.repository.local

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.kenleung.simplemapdemo.model.Delivery

@Dao
interface DeliveryDao {

    @Query("SELECT * FROM delivery ORDER BY id")
    suspend fun getDeliveries(): List<Delivery>

    @Insert(onConflict = REPLACE)
    suspend fun insertAll(deliveries: List<Delivery>)

    @Delete
    suspend fun delete(data: Delivery)

    @Query("DELETE FROM delivery")
    suspend fun deleteAll()
}