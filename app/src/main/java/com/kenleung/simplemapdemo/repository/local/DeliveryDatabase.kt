package com.kenleung.simplemapdemo.repository.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.kenleung.simplemapdemo.model.Delivery

@Database(entities = [Delivery::class], version = 1, exportSchema = false)
abstract class DeliveryDatabase: RoomDatabase() {

    abstract fun getDeliveryDao(): DeliveryDao

    companion object {
        private const val DATABASE_NAME = "database.db"

        @Volatile
        private var INSTANCE: DeliveryDatabase? = null

        // no migration needed

        fun getInstance(context: Context): DeliveryDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DeliveryDatabase::class.java,
                    DATABASE_NAME
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}