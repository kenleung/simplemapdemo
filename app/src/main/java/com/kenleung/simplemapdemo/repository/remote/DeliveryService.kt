package com.kenleung.simplemapdemo.repository.remote

import com.kenleung.simplemapdemo.model.Delivery
import com.kenleung.simplemapdemo.Constants
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface DeliveryService {
    @GET("/deliveries")
    fun getDeliveries(@Query("offset") offset: Int,
                      @Query("limit") limit:Int): Call<List<Delivery>>

    companion object {
        fun create(): DeliveryService {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.API_ENDPOINT)
                .build()

            return retrofit.create(DeliveryService::class.java)
        }
    }
}