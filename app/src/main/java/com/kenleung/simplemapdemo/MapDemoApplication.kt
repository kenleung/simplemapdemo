package com.kenleung.simplemapdemo

import android.app.Application

class MapDemoApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        // Any tool initialisation such as Analytics
    }
}