package com.kenleung.simplemapdemo.view.activity

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.*
import com.kenleung.simplemapdemo.R
import com.kenleung.simplemapdemo.model.Delivery
import com.kenleung.simplemapdemo.model.deliveryContent
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.delivery_content.*



class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var mOrder: Delivery
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        mOrder = intent.getParcelableExtra("Order")

        setupUI()
    }

    private fun setupUI() {
        supportActionBar?.setTitle(R.string.delivery_details)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        bottomSheetBehavior = from(informationView)
        bottomSheetBehavior.state = STATE_EXPANDED
        bottomSheetBehavior.peekHeight = 0
        bottomSheetBehavior.setBottomSheetCallback (object: BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(view: View, position: Float) {
            }

            override fun onStateChanged(view: View, state: Int) {
                when (state) {
                    STATE_COLLAPSED -> mMap.setPadding(0, 0, 0, 0)
                    else -> mMap.setPadding(0, 0, 0, informationView.height)
                }
            }
        })

        textDelivery.text = mOrder.deliveryContent(this)
        Glide.with(this)
            .load(mOrder.imageUrl)
            .apply(RequestOptions.circleCropTransform())
            .into(imageDelivery)

        // Padding for Google Logo
        informationView.post {
            mMap.setPadding(0, 0, 0, informationView.height)
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val position = LatLng(mOrder.location.lat, mOrder.location.lng)

        mMap.addMarker(MarkerOptions().position(position))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(position))
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15f))

        mMap.setOnMarkerClickListener {
            bottomSheetBehavior.state = STATE_EXPANDED
            true
        }

        mMap.setOnMapClickListener {
            bottomSheetBehavior.state = STATE_COLLAPSED
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
