package com.kenleung.simplemapdemo.view.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.kenleung.simplemapdemo.Constants
import com.kenleung.simplemapdemo.R
import com.kenleung.simplemapdemo.model.Delivery
import com.kenleung.simplemapdemo.repository.local.DeliveryDao
import com.kenleung.simplemapdemo.repository.local.DeliveryDatabase
import com.kenleung.simplemapdemo.repository.remote.DeliveryService
import com.kenleung.simplemapdemo.utility.isNetworkConnected
import com.kenleung.simplemapdemo.view.adapter.CustomLoadMoreView
import com.kenleung.simplemapdemo.view.adapter.DeliveryListAdapter
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.CoroutineContext

class MainActivity: AppCompatActivity(), CoroutineScope, BaseQuickAdapter.RequestLoadMoreListener {

    private val job by lazy { Job() }
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    val deliveryAdapter = DeliveryListAdapter(ArrayList())

    private lateinit var deliveryDao: DeliveryDao
    private lateinit var deliveryService: DeliveryService

    // need to keep a offset in case use would delete data
    private var offset = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRepositories()
        initUI()
        refreshDeliveries()
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    private fun initRepositories() {
        deliveryDao = DeliveryDatabase.getInstance(this).getDeliveryDao()
        deliveryService = DeliveryService.create()
    }

    private fun initUI() {
        supportActionBar?.setTitle(R.string.things_to_deliver)
        deliveryRecyclerView.adapter = deliveryAdapter
        val layoutManager = LinearLayoutManager(this)
        deliveryRecyclerView.layoutManager = layoutManager

        deliveryRefreshLayout.setOnRefreshListener {
            refreshDeliveries()
        }

        deliveryAdapter.setLoadMoreView(CustomLoadMoreView())
        deliveryAdapter.setEmptyView(R.layout.empty_view, deliveryRecyclerView)

        deliveryAdapter.setOnItemClickListener { adapter, _, position ->
            val intent = Intent(this@MainActivity, MapsActivity::class.java)
            intent.putExtra("Order", adapter.data[position] as Delivery)
            startActivity(intent)
        }

        deliveryAdapter.setOnItemLongClickListener { _, _, position ->
            deleteDelivery(position)
            true
        }

        deliveryAdapter.setOnLoadMoreListener(this, deliveryRecyclerView)
        deliveryAdapter.openLoadAnimation()
    }

    private val serviceCallback = object: Callback<List<Delivery>> {
        override fun onFailure(call: Call<List<Delivery>>, t: Throwable) {
            deliveryRefreshLayout.isRefreshing = false
            deliveryAdapter.loadMoreFail()
        }

        override fun onResponse(call: Call<List<Delivery>>, response: Response<List<Delivery>>) {
            deliveryRefreshLayout.isRefreshing = false
            if (response.isSuccessful) {
                val responseSize = response.body()?.size ?: 0
                if (responseSize > 0) {
                    response.body()?.let {
                        launch {
                            deliveryDao.insertAll(it)
                            offset = it.last().id + 1
                            deliveryAdapter.addData(it)
                            deliveryAdapter.loadMoreComplete()
                        }
                    }
                } else {
                    // no more data returned from server
                    deliveryAdapter.loadMoreEnd()
                }
            } else {
                deliveryAdapter.loadMoreFail()
            }
        }
    }

    private fun loadLocalDeliveries() {
        launch {
            val localDeliveries = deliveryDao.getDeliveries()
            if (localDeliveries.isNotEmpty()) {
                deliveryAdapter.setNewData(localDeliveries)

                // special case for no network at app launch
                // local data retrieved
                offset = localDeliveries.last().id + 1
            }
        }
    }

    private fun loadRemoteDeliveries() {
        val serviceCall = deliveryService.getDeliveries(offset, Constants.API_DELIVERY_COUNT)
        serviceCall.enqueue(serviceCallback)
    }

    private fun refreshDeliveries() {
        if (this@MainActivity.isNetworkConnected()) {
            launch {
                deliveryDao.deleteAll()
            }
            deliveryRefreshLayout.isRefreshing = true
            offset = 0
            loadRemoteDeliveries()
        } else {
            loadLocalDeliveries()
            deliveryRefreshLayout.isRefreshing = false
        }
        deliveryAdapter.setNewData(ArrayList<Delivery>())
    }

    private fun loadMoreDeliveries() {
        val serviceCall = deliveryService.getDeliveries(offset, Constants.API_DELIVERY_COUNT)
        serviceCall.enqueue(serviceCallback)
    }

    private fun deleteDelivery(position: Int) {
        launch {
            deliveryDao.delete(deliveryAdapter.data[position] as Delivery)
            deliveryAdapter.remove(position)
        }
    }

    override fun onLoadMoreRequested() {
        loadMoreDeliveries()
    }
}
