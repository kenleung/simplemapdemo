package com.kenleung.simplemapdemo.view.adapter

import com.chad.library.adapter.base.loadmore.LoadMoreView
import com.kenleung.simplemapdemo.R


class CustomLoadMoreView : LoadMoreView() {

    override fun getLayoutId(): Int {
        return R.layout.item_load_more
    }

    override fun isLoadEndGone(): Boolean {
        return false
    }

    override fun getLoadingViewId(): Int {
        return R.id.loadMoreLoadingView
    }

    override fun getLoadFailViewId(): Int {
        return R.id.loadMoreFailView
    }

    override fun getLoadEndViewId(): Int {
        return R.id.loadMoreEndView
    }
}