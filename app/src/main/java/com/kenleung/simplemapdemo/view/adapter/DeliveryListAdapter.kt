package com.kenleung.simplemapdemo.view.adapter

import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.kenleung.simplemapdemo.R
import com.kenleung.simplemapdemo.model.Delivery
import com.kenleung.simplemapdemo.model.deliveryContent

class DeliveryListAdapter(data: ArrayList<Delivery>) : BaseQuickAdapter<Delivery, BaseViewHolder>(R.layout.item_delivery, data) {

    override fun convert(helper: BaseViewHolder, current: Delivery) {
        helper.setText(R.id.textDelivery, current.deliveryContent(mContext))
        Glide.with(mContext)
            .load(current.imageUrl)
            .apply(RequestOptions.circleCropTransform())
            .into(helper.getView(R.id.imageDelivery))

//        holder.itemView.setOnClickListener {
//            mListener.onDeliveryClick(deliveries[position])
//        }
//
//        holder.itemView.setOnLongClickListener {
//            mListener.onDeliveryLongClick(deliveries[position])
//            true
//        }
    }
}
