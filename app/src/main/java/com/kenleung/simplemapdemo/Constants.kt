package com.kenleung.simplemapdemo

object Constants {
    const val LIST_THRESHOLD = 5
    const val API_DELIVERY_COUNT = 20
    const val API_ENDPOINT = "https://mock-api-mobile.dev.lalamove.com"
}