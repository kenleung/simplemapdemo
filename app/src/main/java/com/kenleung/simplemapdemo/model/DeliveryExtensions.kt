package com.kenleung.simplemapdemo.model

import android.content.Context
import com.kenleung.simplemapdemo.R

fun Delivery.deliveryContent(context: Context): String {
    return context.getString(R.string.delivery_description, this.description, this.location.address)
}