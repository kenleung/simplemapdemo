package com.kenleung.simplemapdemo

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kenleung.simplemapdemo.repository.local.DeliveryDao
import com.kenleung.simplemapdemo.repository.local.DeliveryDatabase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import java.io.IOException

@RunWith(AndroidJUnit4::class)
@Config(manifest=Config.NONE)
class LocalDataTest {
    private lateinit var deliveryDao: DeliveryDao
    private lateinit var db: DeliveryDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, DeliveryDatabase::class.java).build()
        deliveryDao = db.getDeliveryDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }


    @Test
    fun insertDataTest() = runBlocking {
        val mockDeliveries = DeliveryDatabaseUtil.createDeliveries(Constants.API_DELIVERY_COUNT)
        deliveryDao.insertAll(mockDeliveries)
        val dbDeliveries = deliveryDao.getDeliveries()
        assert(dbDeliveries.size == Constants.API_DELIVERY_COUNT)
    }

    @Test
    fun getDataTest() = runBlocking {
        val mockDeliveries = DeliveryDatabaseUtil.createDeliveries(Constants.API_DELIVERY_COUNT)

        deliveryDao.insertAll(mockDeliveries)

        val dbDeliveries = deliveryDao.getDeliveries()
        assert(dbDeliveries == mockDeliveries.sortedWith(compareBy ( { it.id }, { it.id } )))
    }

    @Test
    fun deleteOneDataTest() = runBlocking {
        val mockDeliveries = DeliveryDatabaseUtil.createDeliveries(Constants.API_DELIVERY_COUNT)
        deliveryDao.insertAll(mockDeliveries)

        val deliveryToBeDelete = mockDeliveries.first()
        deliveryDao.delete(deliveryToBeDelete)
        val dbDeliveries = deliveryDao.getDeliveries()
        assert(dbDeliveries.size == mockDeliveries.size - 1)
    }

    @Test
    fun clearDataTest() = runBlocking {
        val mockDeliveries = DeliveryDatabaseUtil.createDeliveries(Constants.API_DELIVERY_COUNT)
        deliveryDao.insertAll(mockDeliveries)

        deliveryDao.deleteAll()
        val dbDeliveries = deliveryDao.getDeliveries()
        assert(dbDeliveries.isEmpty())
    }
}