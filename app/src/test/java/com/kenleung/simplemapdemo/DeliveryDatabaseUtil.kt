package com.kenleung.simplemapdemo

import com.kenleung.simplemapdemo.model.Delivery
import com.kenleung.simplemapdemo.model.Location

object DeliveryDatabaseUtil {
    fun createDeliveries(count: Int): List<Delivery> {
        return List(count) { Delivery(it, "Deliver money to Ken", "http://anything.com/testing",
            Location("Yuen Long", 23.123, -50.5)) }
    }
}