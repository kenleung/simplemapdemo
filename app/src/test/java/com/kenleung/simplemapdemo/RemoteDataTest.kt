package com.kenleung.simplemapdemo

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kenleung.simplemapdemo.repository.remote.DeliveryService
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import java.io.IOException

@RunWith(AndroidJUnit4::class)
@Config(manifest= Config.NONE)
class RemoteDataTest {
    private lateinit var deliveryService: DeliveryService

    @Before
    fun createService() {
        deliveryService = DeliveryService.create()
    }


    @Test
    @Throws(IOException::class)
    fun getDeliveries() {
        val response = deliveryService.getDeliveries(0, Constants.API_DELIVERY_COUNT).execute()
        assert(response.isSuccessful)
    }
}